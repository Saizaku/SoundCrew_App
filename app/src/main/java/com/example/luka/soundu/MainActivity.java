package com.example.luka.soundu;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        SetListener((Button) findViewById(R.id.button2), (Button) findViewById(R.id.button3)); // zove funkciju
        setTitle("Dobrodosli u SoundU");
    }
    public void SetListener(Button button2, Button button3){
        button2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent newact = new Intent(MainActivity.this, LogIn.class);
                MainActivity.this.startActivity(newact);
            }
        });
        button3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent newact = new Intent(MainActivity.this, SignUp.class);
                MainActivity.this.startActivity(newact);
            }
        });
    } // postavlja listenere za oba buttona





}
