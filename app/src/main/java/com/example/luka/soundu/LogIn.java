package com.example.luka.soundu;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.gson.Gson;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;

public class LogIn extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);
        setTitle("Uloguj se");
        SetListener((Button)findViewById(R.id.button4));
    }
    public void SetListener(Button button)
    {
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //pokuplja sta je korisnik uneo
                String korIme = GetText((TextView)findViewById(R.id.editKorIme));
                String sifra = GetText((TextView)findViewById(R.id.editSifra));
                LogInfo loginf = new LogInfo(korIme,sifra);
                sendRequest(loginf,"link");
                // uloguje se
            }
        });
    }
    public String GetText(TextView editField)
    {
        return editField.getText().toString();
    }
    public void sendRequest(LogInfo loginf, String link){
        Gson gson = new Gson();
        HttpClient httpClient = HttpClientBuilder.create().build();

        try {
            HttpPost request = new HttpPost();
            request.addHeader("Content-type", "application/json");
            StringEntity body = new StringEntity(gson.toJson(loginf));
            request.setEntity(body);
            HttpResponse response = httpClient.execute(request);
            if (response.toString().equalsIgnoreCase("OK")){

            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }

}
