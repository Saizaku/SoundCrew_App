package com.example.luka.soundu;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.params.HttpConnectionParams;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.Console;
import java.io.IOException;
import java.io.InputStream;

public class SignUp extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        setTitle("Kreiraj nalog");
        SetListener((Button) findViewById(R.id.button));
    }
    public void SetListener(Button button)
    {
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //sve promenljive su pokupljene radi slanja bazi
                String mejl = GetText((TextView)findViewById(R.id.editMejl));
                String korIme = GetText((TextView)findViewById(R.id.editKorIme));
                String sifra = GetText((TextView)findViewById(R.id.editSifra));
                String ponovljenasifra = GetText((TextView)findViewById(R.id.editPonoviSifru));
                if(mejl != "" && korIme != "" && sifra != "" && ponovljenasifra != "")
                {
                    if(sifra.equals(ponovljenasifra))
                    {
                        RegisterInfo reginf = new RegisterInfo(mejl,korIme,sifra);
                        sendRequest(reginf,"178.220.165.139:4567/api/v1/register");
                        // prijavi se u bazu podataka
                    }
                }

            }
        });
    }
    public String GetText(TextView editField)
    {
        return editField.getText().toString();
    } // vraca tekst iz nekog editText polja koje mu se posalje

    @SuppressLint("StaticFieldLeak")
    public void sendRequest(final RegisterInfo reginf, final String link){

        //CloseableHttpClient httpClient = HttpClients.createDefault();

                final Gson gson = new GsonBuilder().create();
                final Button button = (Button) findViewById(R.id.button);
                button.setText("?");
                //HttpClient httpClient = new DefaultHttpClient();

        new  AsyncTask<Void,Void,String>() {
            @Override
            protected String doInBackground(Void... voids) {
                try
                {
                    HttpClient httpClient = HttpClientBuilder.create().build();
                    HttpPost request = new HttpPost(link);
                    request.addHeader("Content-type", "application/json");
                    StringEntity body = new StringEntity(gson.toJson(reginf));
                    request.setEntity(body);
                    HttpResponse response = httpClient.execute(request);
                    String message = "";
                    try (InputStream content = response.getEntity().getContent()) {
                        //With apache
                        message = IOUtils.toString(content, "UTF-8");
                        button.setText(message);
                        return message;
                    } catch (UnsupportedOperationException | IOException e) {
                        //Do something here, e.g. LOG
                    }

                } catch(
                Exception e)

                {
                    e.printStackTrace();

                    //System.out.println(e.getStackTrace());
                }

                return null;
            }

            @Override
            protected void onPostExecute(String message){
                if(message.contains("OK")){
                    Intent intent = new Intent(SignUp.this, LogIn.class);
                    SignUp.this.startActivity(intent);
                }
            }

        }.execute();

    }

    /*private class HttpAsyncTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            //sendRequest();
            //nasa func
            return "";
        }
        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
            //procesujemo response ovde
        }
    }*/

}
