package com.example.luka.soundu;

import android.util.Log;

/**
 * Created by Luka on 06-Dec-17.
 */

class LogInfo {
    String korIme;
    String sifra;

    public LogInfo(String unetoKorIme, String unetaSifra)
    {
        korIme = unetoKorIme;
        sifra = unetaSifra;
    }

    public String GetKorIme(){ return korIme; }
    public String GetSifra(){ return sifra; }
}
